import { assert } from 'chai';
import { resolve } from 'path';
import { taroConfig } from '../src';

describe('taroConfig', () => {

  const merge = Object.assign;

  describe('base', () => {
    const mockEnv = {
      PROJECT : 'hello-world',
      MODE    : 'dev',
      TARO_ENV: 'weapp',
      CUSTOM  : 'test',
    };
    const root = resolve(`${__dirname}/misc`);
    const output = resolve(`${__dirname}/misc/output`);
    const packageInfo = require('./misc/package.json');

    const { config, info } = taroConfig({
      root       : root,
      merge,
      projectRoot: (params) => root,
      outputRoot : (params) => output,
      outputPath : (params) => '',
    }, mockEnv);

    it('info test', () => {
      assert.equal(info.project, mockEnv.PROJECT);
      assert.equal(info.mode, mockEnv.MODE);
      assert.equal(info.platform, mockEnv.TARO_ENV);
      assert.equal(info.custom, mockEnv.CUSTOM);
      assert.equal(info.version, packageInfo.version);
      assert.equal(info.projectRoot, resolve(`${__dirname}/misc`));
      assert.equal(info.sourceRoot, resolve(`${__dirname}/misc/src`));
      assert.equal(info.outputRoot, resolve(`${__dirname}/misc/output`));
    });
  });
});
