import { assert } from 'chai';
import { filterExistFiles, isEmptyString, isExistDir, isExistFile, loadJSON } from '../src/utils';
import { resolve } from 'path';


describe('utils', () => {

  describe('isDir', () => {
    it('exists dir', () => {
      assert.isTrue(isExistDir(`${__dirname}/misc/1`));
      assert.isTrue(isExistDir(`${__dirname}/misc/2`));
    });

    it('no exists dir', () => {
      assert.isFalse(isExistDir(`${__dirname}/misc/3`));
    });
  });

  describe('isFile', () => {
    it('exists file', () => {
      assert.isTrue(isExistFile(`${__dirname}/misc/a.txt`));
      assert.isTrue(isExistFile(`${__dirname}/misc/b.txt`));
    });

    it('no exists file', () => {
      assert.isFalse(isExistFile(`${__dirname}/misc/c.txt`));
    });
  });

  describe('loadJSON', () => {
    it('exists json', () => {
      assert.deepEqual(loadJSON(`${__dirname}/misc/test.json`), { a: 'a' });
    });

    it('no exists json', () => {
      assert.equal(loadJSON(`${__dirname}/misc/test-1.json`), false);
    });
  });

  describe('filterExistFiles', () => {
    it('exists files', () => {
      const files = [
        `${__dirname}/misc/a.txt`,
        `${__dirname}/misc/b.txt`,
        `${__dirname}/misc/c.txt`,
      ];
      const rs = filterExistFiles(files);
      assert.equal(rs.length, 2);
      assert.equal(rs[0], resolve(files[0]));
      assert.equal(rs[1], resolve(files[1]));
    });
  });

  describe('isEmptyString', () => {
    it('empty item', () => {
      assert.isTrue(isEmptyString(undefined));
      assert.isTrue(isEmptyString(null));
      assert.isTrue(isEmptyString(''));
    });

    it('not string', () => {
      assert.isTrue(isEmptyString({}));
      assert.isTrue(isEmptyString([]));
      assert.isTrue(isEmptyString(123));
    });

    it('string', () => {
      assert.isFalse(isEmptyString('  '));
      assert.isFalse(isEmptyString('asdgqwe'));
    });
  });
});
