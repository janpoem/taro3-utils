export = {
  projectName    : '',
  date           : '',
  designWidth    : 750,
  deviceRatio    : {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
  },
  env            : {},
  sourceRoot     : '',
  outputRoot     : '',
  plugins        : [],
  defineConstants: {},
  copy           : {
    patterns: [],
    options : {},
  },
  framework      : 'react',
  mini           : {
    postcss: {
      // @see https://www.npmjs.com/package/postcss-pxtransform
      // 待定，这个 px 转换，并不是通用的，很搞怪
      // pxtransform: {
      //   enable: true,
      //   export: {},
      // },
      // @see https://www.npmjs.com/package/postcss-url
      url       : {
        enable: true,
        config: {
          // 小程序引入图片，必须直接调用远端 CDN 的
          // 本身小程序打包能包含的图片资源有限，不同平台对小程序打包的尺寸存在不同的限制，
          // 所以尽量不在小程序内放 png，尽可能是 svg （也因为手机视网膜屏导致分辨率极度的变大，png jpg 图片更加不适合小程序，往往需要2倍以上）
          // 所以去掉 url 长度限制的要求
          // limit: 1024, // 设定转换尺寸上限
        },
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern     : 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]',
        },
      },
    },
  },
  h5             : {
    publicPath     : '/',
    staticDirectory: 'static',
    postcss        : {
      autoprefixer: {
        enable: true,
        config: {},
      },
      cssModules  : {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern     : 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]',
        },
      },
    },
  },
};
