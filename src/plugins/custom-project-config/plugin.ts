import {
  CustomProjectConfig,
  CustomProjectConfigOptions,
  TaroPluginContextSimplify,
} from './index';

export = (
  ctx: TaroPluginContextSimplify,
  options: CustomProjectConfigOptions,
) => {
  const plugin = new CustomProjectConfig(ctx, options);
  // plugin 主体
  ctx.onBuildStart(() => {
    if (plugin.shouldGenerateConfigFile) {
      plugin.initConfig();
    }
  });
  ctx.onBuildFinish(() => {
    if (plugin.shouldGenerateConfigFile && plugin.isChanged) {
      plugin.writeProjectConfig();
    }
  });
};
