import { resolve } from 'path';
import { CustomProjectConfigOptions } from './custom-project-config';

export const customProjectConfig = (opts: CustomProjectConfigOptions): [string, CustomProjectConfigOptions] => {
  return [
    resolve(`${__dirname}/custom-project-config/plugin`),
    opts,
  ];
};
