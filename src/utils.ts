import { lstatSync, readFileSync } from 'fs';

/**
 * 是否是一个存在的目录
 *
 * @param dir
 */
export const isExistDir = (dir: string): boolean => {
  try {
    return lstatSync(dir).isDirectory();
  } catch (e) {
    return false;
  }
};

/**
 * 是否是一个存在的文件
 *
 * @param file
 */
export const isExistFile = (file: string): boolean => {
  try {
    return lstatSync(file).isFile();
  } catch (e) {
    return false;
  }
};

/**
 * 加载 JSON 数据，返回一个 object 的数据类型（数组或object），如果不存文件，或者 JSON 解析出错，则返回 false
 *
 * @param file
 */
export const loadJSON = (file: string): Record<string, any> | false => {
  try {
    if (isExistFile(file)) {
      const buffer = readFileSync(file);
      // @ts-ignore 这里 JSON.parse 是允许传入 buffer 的，没必要再做一个转换到 string
      const json = JSON.parse(buffer.toString());
      if (typeof json === 'object') {
        return json as Record<string, any>;
      }
    }
    return false;
  } catch (e) {
    return false;
  }
};

/**
 * 过滤存在的文件列表
 *
 * @param files
 */
export const filterExistFiles = (files: string[]): string[] => files.filter(isExistFile);

/**
 * 不为空的字符
 *
 * @param value
 */
export const isEmptyString = (value?: any): boolean => {
  return value == null || typeof value !== 'string' || value === '';
};
