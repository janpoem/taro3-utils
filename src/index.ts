export { taroConfig } from './taro-config';
export { customProjectConfig } from './plugins';
export { deepMerge, ArrayMergeMode } from './deep-merge';

export { isExistDir, isExistFile, isEmptyString, filterExistFiles } from './utils';
export { extractProjectInfo, parseNpmCommand, RuntimeMode, filterRuntimeMode } from './project-info';
